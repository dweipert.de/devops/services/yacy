import sys, os
import argparse
import re


parser = argparse.ArgumentParser(
    prog="config"
)
parser.add_argument("env_file")
parser.add_argument("yacy_conf")
args = parser.parse_args()


env_file = open(args.env_file, "r")
with open(args.yacy_conf, "r") as file: yacy_conf = file.read()

for line in env_file:
    if line.startswith("YACY_"):
        env_key_value = line.split("=")
        yacy_key = env_key_value[0][5:]
        yacy_value = env_key_value[1]

        pattern = re.compile(r"(%s=)(.*)" % yacy_key)
        yacy_conf = pattern.sub(r"\1%s" % yacy_value, yacy_conf)

with open(args.yacy_conf, "w+") as file: file.write(yacy_conf)
